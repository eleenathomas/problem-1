package com.company;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBWrite
{
    DBConnect dbWriteConnect= new DBConnect();
    Connection con= dbWriteConnect.connect();

    public void FileToDB(String name, int size, String content) throws SQLException
    {
        String query;
        query= "INSERT INTO FileDetials VALUES(?,?,?)";

        PreparedStatement preparedStatement= con.prepareStatement(query);

            preparedStatement.setString(1,name);
            preparedStatement.setInt(2,size);
            preparedStatement.setString(3,content);

            preparedStatement.execute();

    }
}

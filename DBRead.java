package com.company;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBRead extends FileRead implements Runnable
{
    DBConnect dbConnect= new DBConnect();
    Connection con= dbConnect.connect();

    ArrayList<String> fileList, getFileList;

    public boolean whileChecker;

    public DBRead() throws SQLException
    {
        DBFetch dbf= new DBFetch();
        dbf.FetchFromDB();
    }
    @Override
    public void fileReadWrite() {

        while (!whileChecker)
        {
            DBFetch dbFetch = new DBFetch();
            try
            {
                getFileList = dbFetch.FetchFromDB();
                for (String dbFile : getFileList) {
                    if (!fileList.contains(dbFile)) {
                        System.out.println(dbFile);
                        fileList.add(dbFile);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }
}

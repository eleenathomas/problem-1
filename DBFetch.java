package com.company;

import java.sql.*;
import java.util.ArrayList;

public class DBFetch
{
    DBConnect dbFetchConnect= new DBConnect();
    Connection con= dbFetchConnect.connect();


    ArrayList<String> arrayList= new ArrayList<String>();

    public ArrayList<String> FetchFromDB() throws SQLException
    {

        String query = "SELECT FileName FROM FileDetials";

        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);

        while (rs.next()) {
            arrayList.add(rs.getString("FileName"));
        }

        return arrayList;
    }
}


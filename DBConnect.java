package com.company;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnect
{
    public Connection conn;

    public Connection connect()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/FileSystem?autoReconnect=true&useSSL=false", "root", "root");
        }
        catch (Exception e)
        {
            System.out.println("ERROR" + e);
        }
        return conn;
    }
}

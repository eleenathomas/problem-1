package com.company;

import java.io.*;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class FileRead implements FileBehaviour, Runnable
{
    DBConnect dbFRConnect= new DBConnect();
    Connection con= dbFRConnect.connect();

    private boolean whileChecker;

    ArrayList<String> fetchedFileList;

    BufferedReader br = null;

    String content= null;


    public FileRead() throws SQLException
    {
        DBFetch dbFetch= new DBFetch();
        fetchedFileList = dbFetch.FetchFromDB();
    }

    public void run()
    {
        while (!whileChecker)
        {
            DBFetch dbw = new DBFetch();
            try
            {
                dbw.FetchFromDB();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }

                File f = new File("/home/training/IdeaProjects/Files");

                File[] fileArray = f.listFiles();
                {
                    for (File file : fileArray)
                    {
                        if (!fetchedFileList.contains(file.getName()))
                        {
                            fetchedFileList.add(file.getName());

                            if (!file.exists() || !file.isFile() && !file.getName().endsWith(".txt"))
                                return;

                            try
                            {
                                br = new BufferedReader(new FileReader(file));
                                content = br.readLine();
                            } catch (FileNotFoundException e)
                            {
                                System.out.print(e);
                            }
                            catch (IOException i)
                            {
                                System.out.print(i);
                            }

                            String name = file.getName();
                            int size = (int) file.length();

                            DBWrite dbWrite = new DBWrite();
                            try {
                                dbWrite.FileToDB(name, size, content);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
        }
    }

}

